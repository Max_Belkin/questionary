from django import forms
from django.contrib.auth.models import User

from questionary.models import (
    Question, QuestionAnswer, Questionary, AnswerHistory, QuestionaryQuestion
)


class QuestionForm(forms.Form):

    def __init__(self, questionary_slug=None, user=None, *args, **kwargs):
        super(QuestionForm, self).__init__(*args, **kwargs)
        if not user:
            return

        question_available_ids = self._get_available_questions(
            questionary_slug, user
        )
        if not question_available_ids:
            self.questionary_answered = True
            return
        questions = Question.objects.filter(
            id__in=question_available_ids,
        )
        # It would be slow on fat questionnaires
        top_question = max(questions, key=lambda q: q.weight)

        if questionary_slug and top_question.type == 1:
            self.fields['answer'] = forms.ChoiceField(
                label=top_question.text,
                choices=QuestionAnswer.objects.filter(
                    question=top_question
                ).values_list('id', 'answer')
            )
        elif questionary_slug and top_question.type == 2:
            self.fields['answer'] = forms.MultipleChoiceField(
                label=top_question.text,
                choices=list(QuestionAnswer.objects.filter(
                    question=top_question
                ).values_list('id', 'answer'))
            )

    @staticmethod
    def _get_available_questions(questionary_slug, user):
        questionary_obj = Questionary.objects.get(slug=questionary_slug)

        # look for a sufficient question in the questionary
        answered = set(
            AnswerHistory.objects.filter(user=user).values_list(
                'answer_given__question_id', flat=True
            )
        )
        question_available_ids = set(
            QuestionaryQuestion.objects.filter(
                questionary=questionary_obj
            ).exclude(
                question__in=answered
            ).values_list('question_id', flat=True)
        )
        return question_available_ids


class QuestionBrowseForm(forms.Form):

    def __init__(self, questionary_slug, number, *args, **kwargs):
        super(QuestionBrowseForm, self).__init__(*args, **kwargs)
        questionary_obj = Questionary.objects.get(slug=questionary_slug)
        question = QuestionaryQuestion.objects.filter(
            questionary=questionary_obj)[number-1].question

        if question.type == 1:
            self.fields['answer'] = forms.ChoiceField(
                label=question.text,
                choices=QuestionAnswer.objects.filter(
                    question=question
                ).values_list('id', 'answer'),
                required=False
            )
        elif question.type == 2:
            self.fields['answer'] = forms.MultipleChoiceField(
                label=question.text,
                choices=list(QuestionAnswer.objects.filter(
                    question=question
                ).values_list('id', 'answer')),
                required=False
            )


class NewQuestionaryForm(forms.Form):
    name = forms.CharField(max_length=255)
    slug = forms.CharField(max_length=45)


class NewQuestionForm(forms.Form):
    question_text = forms.CharField(
        widget=forms.Textarea(attrs={
            'rows': 3,
            'cols': 45,
        }),
        max_length=1000,
        required=True
    )
    multiple_type = forms.BooleanField(required=False)

    def __init__(self, *args, **kwargs):
        super(NewQuestionForm, self).__init__(*args, **kwargs)

        for i in range(1, 10):
            self.fields['option_%s' % i] = forms.CharField(
                widget=forms.Textarea(attrs={
                    'rows': 2,
                    'cols': 100,
                }),
                max_length=255,
                required=False
            )


class SetWeightForm(forms.Form):

    def __init__(self, *args, **kwargs):
        super(SetWeightForm, self).__init__(*args, **kwargs)
        self.fields['user'] = forms.ChoiceField(
            choices=list(User.objects.all().exclude(
                username='admin').values_list('id', 'username')),
            required=True
        )
        self.fields['weight'] = forms.IntegerField(
            initial=1,
            required=True
        )
