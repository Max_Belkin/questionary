# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('text', models.TextField()),
                ('type', models.IntegerField()),
            ],
            options={
                'db_table': 'question',
            },
        ),
        migrations.CreateModel(
            name='QuestionAnswer',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('answer', models.CharField(max_length=255)),
                ('times_voted', models.IntegerField(default=0)),
                ('question', models.ForeignKey(to='questionary.Question')),
            ],
            options={
                'db_table': 'question_answer',
            },
        ),
        migrations.CreateModel(
            name='Questionary',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('slug', models.CharField(unique=True, max_length=45)),
                ('weight', models.IntegerField(null=True)),
            ],
            options={
                'db_table': 'questionary',
            },
        ),
        migrations.CreateModel(
            name='QuestionaryQuestion',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('question', models.ForeignKey(to='questionary.Question')),
                ('questionary', models.ForeignKey(to='questionary.Questionary')),
            ],
            options={
                'db_table': 'questionary_question',
            },
        ),
    ]
