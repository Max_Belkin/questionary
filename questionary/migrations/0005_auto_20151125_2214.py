# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('questionary', '0004_userweight'),
    ]

    operations = [
        migrations.AlterModelTable(
            name='userweight',
            table='user_weight',
        ),
    ]
