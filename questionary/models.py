from django.db import models
from django.contrib.auth.models import User


class Questionary(models.Model):
    user = models.ForeignKey(User)
    name = models.CharField(max_length=255)
    slug = models.CharField(max_length=45, unique=True)
    weight = models.IntegerField(null=True)

    @property
    def author_weight(self):
        return UserWeight.objects.get(user=self.user).weight

    @property
    def popularity(self):
        questions = set(QuestionaryQuestion.objects.filter(
            questionary=self).values_list('question_id', flat=True))
        question_qty = len(questions)
        all_questionary_answers = AnswerHistory.objects.filter(
            answer_given__question_id__in=questions)
        # users_participated = len(set(
        #     all_questionary_answers.values_list('user', flat=True)))
        answers_qty = len(set(
            all_questionary_answers.values_list(
                'user', 'answer_given__question')
        ))
        if question_qty == 0:
            return 0.0
        return float(answers_qty/question_qty)

    class Meta:
        db_table = 'questionary'


class Question(models.Model):
    text = models.TextField()
    type = models.IntegerField()

    @property
    def weight(self):
        # calculate dynamic weight of the question.
        # it is used to define the order of the questions
        user_answered_qty = AnswerHistory.objects.filter(
            answer_given__question=self
        ).values_list('user_id', flat=True)
        return len(set(user_answered_qty))

    class Meta:
        db_table = 'question'


class QuestionaryQuestion(models.Model):
    questionary = models.ForeignKey(Questionary)
    question = models.ForeignKey(Question)

    class Meta:
        db_table = 'questionary_question'


class QuestionAnswer(models.Model):
    question = models.ForeignKey(Question)
    answer = models.CharField(max_length=255)
    # Should I consider difference between times voted and times shown?
    times_voted = models.IntegerField(default=0)

    class Meta:
        db_table = 'question_answer'


class AnswerHistory(models.Model):
    user = models.ForeignKey(User)
    answer_given = models.ForeignKey(QuestionAnswer)

    class Meta:
        db_table = 'answer_history'


class UserWeight(models.Model):
    user = models.ForeignKey(User)
    weight = models.IntegerField(default=1)

    class Meta:
        db_table = 'user_weight'
