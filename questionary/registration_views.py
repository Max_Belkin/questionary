from registration.backends.simple.views import RegistrationView


class RedirectRegistrationView(RegistrationView):
    def get_success_url(self, request=None, user=None):
        return 'home'
