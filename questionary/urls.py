from django.conf.urls import patterns, url

from questionary import views


urlpatterns = patterns(
    '',
    url(r'^stat/$', views.StatQuestionaryListView.as_view(), name='stat'),
    url(r'^success/$', views.OperationSucceededView.as_view(), name='success'),
    url(r'^set_weight/$', views.SetWeightView.as_view(), name='set_weight'),
    url(r'^new_question/(?P<user>[a-zA-Z0-9_]+)/(?P<slug>[a-zA-Z0-9_]+)/$',
        views.NewQuestionView.as_view(),
        name='new_question'),
    url(r'^start_new/$', views.NewQuestionaryView.as_view(),
        name='start_new_questionary'),
    url(r'^browse/(?P<questionary_slug>[a-zA-Z0-9_]+)/(?P<number>\d+)$',
        views.QuestionBrowseView.as_view(),
        name='questionary_browse_next_question'),
    url(r'^answer/(?P<questionary_slug>[a-zA-Z0-9_]+)/next$',
        views.QuestionView.as_view(),
        name='questionary_next_question'),
    url(r'^answer/(?P<slug>[a-zA-Z0-9_]+)/$', views.QuestionaryView.as_view(),
        name='questionary'),
    url(r'^(?P<username>[a-zA-Z0-9_@\+\.-]+)/$', views.UserListView.as_view(),
        name='user_qs'),
)