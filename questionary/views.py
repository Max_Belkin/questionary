from itertools import chain

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404
from django.http.response import HttpResponseForbidden
from django.views.generic import ListView, DetailView, FormView, TemplateView

from questionary.forms import (
    QuestionForm, NewQuestionaryForm, NewQuestionForm, SetWeightForm,
    QuestionBrowseForm
)
from questionary.models import (
    Questionary, QuestionAnswer, AnswerHistory, Question, QuestionaryQuestion,
    UserWeight
)


class LoginRequiredMixin(object):
    @classmethod
    def as_view(cls, **kwargs):
        view = super(LoginRequiredMixin, cls).as_view(**kwargs)
        return login_required(view)


class HomePageView(ListView):
    template_name = 'home.html'
    model = Questionary

    def get_queryset(self):
        all_questionary = self.model.objects.all()
        weighted = sorted(
            all_questionary.exclude(weight=None).order_by('-weight'),
            key=lambda x: x.author_weight*x.weight,
            reverse=True
        )

        not_weighted = all_questionary.filter(weight=None).order_by('?')
        ordered_quests = chain(weighted, not_weighted)

        return ordered_quests


class UserListView(ListView):
    model = Questionary
    template_name = 'user_list.html'

    def get_queryset(self):
        u = get_object_or_404(User, username=self.kwargs['username'])
        for_user = self.model.objects.filter(user=u)
        weighted = for_user.exclude(weight=None).order_by('weight')
        not_weighted = for_user.filter(weight=None).order_by('?')

        ordered_quests = chain(weighted, not_weighted)
        return ordered_quests


class QuestionaryView(DetailView):
    model = Questionary
    template_name = 'questionary.html'

    def get_queryset(self):
        questionary = self.model.objects.filter(slug=self.kwargs['slug'])
        return questionary

    def get_context_data(self, **kwargs):
        ctx = super(QuestionaryView, self).get_context_data(**kwargs)
        ctx['passed'] = False
        return ctx


class NewQuestionaryView(LoginRequiredMixin, FormView):
    form_class = NewQuestionaryForm
    template_name = 'new_questionary.html'

    def form_valid(self, form):
        self.new_questionary_slug = form.cleaned_data.pop('slug')
        new_questionary_name = form.cleaned_data.pop('name')
        questionary = Questionary.objects.create(
            name=new_questionary_name,
            slug=self.new_questionary_slug,
            user=self.request.user
        )
        for key, question_text in form.cleaned_data.items():
            if question_text and key.startswith('question_'):
                if form.cleaned_data['multiple_type_%s' % key[-1]]:
                    question_type = 2
                else:
                    question_type = 1
                question = Question.objects.create(
                    text=question_text,
                    type=question_type
                )
                QuestionaryQuestion.objects.create(
                    questionary=questionary,
                    question=question
                )

        return super(NewQuestionaryView, self).form_valid(form)

    def get_success_url(self):
        return reverse(
            'new_question',
            kwargs={
                'slug': self.new_questionary_slug,
                'user': self.request.user
            }
        )


class NewQuestionView(LoginRequiredMixin, FormView):
    form_class = NewQuestionForm
    template_name = 'new_question.html'

    def get(self, request, *args, **kwargs):
        questionary = get_object_or_404(Questionary, slug=self.kwargs['slug'])
        if questionary.user != request.user:
            return HttpResponseForbidden()
        return super(NewQuestionView, self).get(request, *args, **kwargs)

    def form_valid(self, form):
        questionary = get_object_or_404(Questionary, slug=self.kwargs['slug'])
        new_question = Question.objects.create(
            text=form.cleaned_data.pop('question_text'),
            type=2 if form.cleaned_data.pop('multiple_type') else 1
        )
        QuestionaryQuestion.objects.create(
            questionary=questionary,
            question=new_question
        )
        for key, answer_text in form.cleaned_data.items():
            if answer_text:
                question_answer = QuestionAnswer.objects.create(
                    question=new_question,
                    answer=answer_text
                )

        return super(NewQuestionView, self).form_valid(form)

    def get_success_url(self):
        return reverse(
            'new_question',
            kwargs={
                'user': self.request.user,
                'slug': self.kwargs['slug']
            }
        )


class QuestionView(LoginRequiredMixin, FormView):
    form_class = QuestionForm
    template_name = 'question.html'

    def get_success_url(self):
        return reverse(
            'questionary_next_question',
            kwargs={'questionary_slug': self.kwargs['questionary_slug']}
        )

    def get_form_kwargs(self):
        kwargs = super(QuestionView, self).get_form_kwargs()
        kwargs.update({
            'questionary_slug': self.kwargs['questionary_slug'],
            'user': self.request.user,
        })
        return kwargs

    def form_valid(self, form):
        answers = form.cleaned_data.get('answer')
        if isinstance(answers, list):
            pass
        elif isinstance(answers, str):
            answers = [answers, ]
        for answer in answers:
            answer_obj = QuestionAnswer.objects.get(id=answer)
            answer_obj.times_voted += 1
            answer_obj.save()

            AnswerHistory.objects.create(
                user=self.request.user,
                answer_given=answer_obj
            )
        return super(QuestionView, self).form_valid(form)


class QuestionBrowseView(FormView):
    form_class = QuestionBrowseForm
    template_name = 'question_browse.html'
    next_number = None

    def get(self, request, *args, **kwargs):
        self._set_current_number()
        return super(QuestionBrowseView, self).get(request, *args, **kwargs)

    def get_success_url(self):
        self._set_current_number()

        return reverse(
            'questionary_browse_next_question',
            kwargs={
                'questionary_slug': self.kwargs['questionary_slug'],
                'number': self.next_number
            }
        )

    def get_form_kwargs(self):
        kwargs = super(QuestionBrowseView, self).get_form_kwargs()
        kwargs.update({
            'questionary_slug': self.kwargs['questionary_slug'],
            'number': self.next_number or 1,
        })
        return kwargs

    def form_invalid(self, form):
        # Deny any errors
        return super(QuestionBrowseView, self).form_valid(form)

    def _set_current_number(self):
        questionary = Questionary.objects.get(
            slug=self.kwargs['questionary_slug'])
        questions_qty = QuestionaryQuestion.objects.filter(
            questionary=questionary
        ).count()
        if questions_qty:
            self.next_number = (
                (int(self.kwargs['number']) + 1) % (questions_qty + 1)
            )
            if self.next_number == 0:
                self.next_number = 1
        else:
            self.next_number = 1


class SetWeightView(LoginRequiredMixin, FormView):
    form_class = SetWeightForm
    template_name = 'set_user_weight.html'

    def get(self, request, *args, **kwargs):
        if request.user.username != 'admin':
            return HttpResponseForbidden()
        return super(SetWeightView, self).get(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('success')

    def form_valid(self, form):
        user_weight = UserWeight.objects.get(user_id=form.cleaned_data['user'])
        user_weight.weight = form.cleaned_data['weight']
        user_weight.save()
        return super(SetWeightView, self).form_valid(form)


class OperationSucceededView(TemplateView):
    template_name = 'success.html'


class StatQuestionaryListView(LoginRequiredMixin, ListView):
    model = Questionary
    template_name = 'stat.html'

    def get_queryset(self):

        ordered_quests = self.model.objects.all()
        if self.request.user.username != 'admin':
            ordered_quests = ordered_quests.filter(
                user=self.request.user)
        ordered_quests = sorted(ordered_quests, key=lambda x: x.popularity,
                                reverse=True)
        return ordered_quests

    def get_context_data(self, **kwargs):
        ctx = super(StatQuestionaryListView, self).get_context_data(**kwargs)
        if self.request.user.username == 'admin':
            users_qty = User.objects.all().count()
            quests_qty = Questionary.objects.all().count()
            ctx['weighted_users'] = UserWeight.objects.all().order_by(
                '-weight')
            ctx['quests_per_user'] = "{0:.2f}".format(
                float(quests_qty/users_qty))
            all_questionary_answers = AnswerHistory.objects.all()
            answers_qty = len(set(
                all_questionary_answers.values_list(
                    'user', 'answer_given__question')
            ))
            ctx['answers_per_user'] = "{0:.2f}".format(
                float(answers_qty / users_qty))
            ctx['answers_per_quest'] = "{0:.2f}".format(
                float(answers_qty / quests_qty))
        return ctx
