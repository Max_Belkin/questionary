from django.conf.urls import url, include, patterns

from questionary import views as q_views
from questionary import registration_views as reg_views


urlpatterns = patterns(
    '',
    url(r'^$', q_views.HomePageView.as_view(), name='home'),
    url(r'^accounts/register/$', reg_views.RedirectRegistrationView.as_view(),
        name='registration_register'),
    url(r'^accounts/', include('registration.backends.simple.urls')),
    url(r'^questionary/', include('questionary.urls')),
)
